package in.sukhothai.parkingmanagementapp;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.util.DisplayMetrics;
import android.util.Log;
///hours in circle

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.dantsu.escposprinter.BuildConfig;
import com.dantsu.escposprinter.connection.DeviceConnection;
import com.dantsu.escposprinter.connection.tcp.TcpConnection;
import com.dantsu.escposprinter.textparser.PrinterTextParserImg;

import com.epson.epos2.printer.Printer;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;

/*import com.google.firebase.ml.vision.text.FirebaseVisionTextDetector;*/


import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import id.zelory.compressor.Compressor;
import in.sukhothai.parkingmanagementapp.POJO.VehicleCategoryRecyclerAdapter;
import in.sukhothai.parkingmanagementapp.POJO.VehicleCategoryRecyclerAdapterPojo;
import in.sukhothai.parkingmanagementapp.RoomDataBase.BillingDaoList;
import in.sukhothai.parkingmanagementapp.RoomDataBase.BillingDataBase;
import in.sukhothai.parkingmanagementapp.RoomDataBase.ParkingDataBase;
import in.sukhothai.parkingmanagementapp.RoomDataBase.ParkingList;
import in.sukhothai.parkingmanagementapp.Utilities.Constants;
import in.sukhothai.parkingmanagementapp.Utilities.IOUtils;
import in.sukhothai.parkingmanagementapp.Utilities.MyLog;
import in.sukhothai.parkingmanagementapp.Utilities.SharedPrefUtil;
import in.sukhothai.parkingmanagementapp.Utilities.SubItemRecyclerListener;
import in.sukhothai.parkingmanagementapp.async.AsyncEscPosPrinter;
import in.sukhothai.parkingmanagementapp.async.AsyncTcpEscPosPrint;
import in.sukhothai.parkingmanagementapp.network.NetworkUtils;
import in.sukhothai.parkingmanagementapp.network.NetworkUtilsReceiver;
import mehdi.sakout.fancybuttons.FancyButton;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;


@SuppressWarnings("ALL")
public class AddVehicleFragment extends Fragment implements CompoundButton.OnCheckedChangeListener, NetworkUtilsReceiver.NetworkResponseInt {
    private Spinner addCategorySpinner, addparkingTypeSpinner;
    private static String TAG = "SecondFragment";
    private static ParkingDataBase myDatabase;
    String[] ParkingTypeList = {"Per Hour", "Over Night"};
    private IOUtils ioUtils;
    private Location location;
    private static List<BillingDaoList> billingdaolist;
    private Switch aSwitch;
    private Double LAT, LON;
    public StringBuilder stringBuilder;
    private String forprint;
    private boolean clicked = false;
    public static ArrayList<String> VehicleList = new ArrayList<>();
    public static ArrayList<String> VehicleList2 = new ArrayList<>();
    static BillingDaoList billingDataList;
    private EditText vehicleNumber, vehicleOwner, mobile;
    private FancyButton submitBTN;
    private ImageView vehicleImg, ownerIDImg, overnightIMAGEVIEW, perHourIMAGEVIEWhite;
    private CardView cardViewperhour, cardViewovernight;
    private TextView openCameraTXT, vehicleTypeperhour, vehicleTypeovernight, addprice, captureTXT;
    private ImageView vehicleNumberimageVIEW, detectNumberPlateImageView;
    private LinearLayout capturEcarImageVIEW;
    private static int CAMERA_REQUEST_CODE = 1000;
    private static int IMAGE_PICK_CODE = 1001;
    private RecyclerView parkinGvehicletypeRECYLERVIEW;
    private String filename, latString, longString, imageFilePathID, uploadgalleryurl, vehiclEtype, currentTime, indateandtime, fulldateformat, dayOfTheWeek, today,
            seletedParkingType, finaladdcharges, vehicleNumberSTRING, mobileSTRING, parkInTM, inDay, parkingInDt, vehicleNumber_Print,
            headerDate, addCharges, iPaddresS, perHour, price, overNight, parkingType, currentdate, finalSrno, iPaddress, getVehicleType;
    private File dest, sourceFile;
    private TextView textView;
    private String ownerIDurl = "";
    private SurfaceView surfaceView;
    private static VehicleCategoryRecyclerAdapter vehicleCategoryRecyclerAdapter;
    private static VehicleCategoryRecyclerAdapterPojo vehicleCategoryRecyclerAdapterPojo;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    Bitmap imageBitmap;
    private JSONObject printobject;
    private LinearLayout ownerIDLinearlayout;
    private TextToSpeech textToSpeech;
    private String stringResult = null;
    private static int srcounter = 0;
    private static int srNo = 0;
    static RecyclerView recyclerViewFilter;
    static GridLayoutManager linearLayoutManager;
    //  static ProgressBar progressBarFilter;
    private Context ctx;
    private Uri photoURI, cropUri, imageUri;
    private static Printer mPrinter = null;
    private static String mainobjectVehicleMaster;
    String clickType = "";
    String srNoFinal, imageFilePath, vehicleOwnerSTRING, imageFilePathGallery = "";
    public int serialno;
    private static Dialog dialogTherap;
    private ImageView closeHolderIMG;
    private TextInputEditText vehicleNumberET;
    private LinearLayout dateHolder;
    private FancyButton dateFB, searchBTN;
    static BillingDataBase billingDatabase;
    private static Context context;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_vehical, container, false);
        myDatabase = Room.databaseBuilder(getActivity(), ParkingDataBase.class, "ParkingDb").allowMainThreadQueries().build();
        billingDatabase = Room.databaseBuilder(getActivity(), BillingDataBase.class, "BillingVehiclePojo").allowMainThreadQueries().build();

        try {
            billingDataList = new BillingDaoList();
            VehicleList = new ArrayList<>();

         /*   VehicleList.add("AUTO");
            VehicleList.add("BUS");
            VehicleList.add("CAR");
            VehicleList.add("CYCLE");
            VehicleList.add("MINI BUS");
            VehicleList.add("SUV");
            VehicleList.add("TEMPO");
            VehicleList.add("2 WHEELER");
         */

            context = getActivity();
            seletedParkingType = "Per Hour";
            addparkingTypeSpinner = view.findViewById(R.id.addparkingType);
            ownerIDLinearlayout = view.findViewById(R.id.ownerIDLinearlayout);
            vehicleNumber = view.findViewById(R.id.vehicleNumber);
            mobile = view.findViewById(R.id.mobile);
            vehicleOwner = view.findViewById(R.id.vehicleOwner);
            addparkingTypeSpinner = view.findViewById(R.id.addparkingType);
            submitBTN = view.findViewById(R.id.submitBTN);
            vehicleImg = view.findViewById(R.id.vehicleImg);
            ownerIDImg = view.findViewById(R.id.ownerIDImg);
            overnightIMAGEVIEW = view.findViewById(R.id.overnightIMAGEVIEW);
            perHourIMAGEVIEWhite = view.findViewById(R.id.perHourIMAGEVIEWhite);
            recyclerViewFilter = view.findViewById(R.id.recyclerViewFilter);
            surfaceView = view.findViewById(R.id.surfaceView);
            //          progressBarFilter = view.findViewById(R.id.progressBarFilter);
            vehicleNumberimageVIEW = view.findViewById(R.id.vehicleNumberimageVIEW);
            getdate();
            cardViewovernight = view.findViewById(R.id.cardViewovernight);
            cardViewperhour = view.findViewById(R.id.cardViewperhour);
            vehicleTypeperhour = view.findViewById(R.id.vehicleTypeperhour);
            vehicleTypeovernight = view.findViewById(R.id.vehicleTypeovernight);
            addprice = view.findViewById(R.id.addprice);
            captureTXT = view.findViewById(R.id.captureTXT);

            aSwitch = (Switch) view.findViewById(R.id.simpleSwitch);
            setHasOptionsMenu(true);
            if (aSwitch != null) {
                aSwitch.setOnCheckedChangeListener(this);
            }

            getVehicleType();
            setGetVehicleTypeOffline();
            addprice.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), BillingActivity.class);
                    startActivity(intent);
                }
            });
            //        detectNumberPlateImageView = view.findViewById(R.id.detectNumberPlateImageView);
            capturEcarImageVIEW = view.findViewById(R.id.capturEcarImageVIEW);
            recyclerViewFilter.setHasFixedSize(true);
            linearLayoutManager = new GridLayoutManager(getContext(), 2);
            linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
            recyclerViewFilter.setLayoutManager(linearLayoutManager);

            /// sttic
            /*vehicleCategoryRecyclerAdapter.selected_position = -1;
            vehicleCategoryRecyclerAdapter = new VehicleCategoryRecyclerAdapter(VehicleList, getActivity());
            recyclerViewFilter.setAdapter(vehicleCategoryRecyclerAdapter);
            vehicleCategoryRecyclerAdapter.notifyDataSetChanged();
      */
            vehiclEtype = "";
            ctx = getActivity();
            location = SharedPrefUtil.getLocation(getActivity());
            LAT = location.getLatitude();
            LON = location.getLongitude();
            latString = String.valueOf(LAT);
            longString = String.valueOf(LON);
            //    printRecieptData();
            cardViewperhour.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_gray));

            vehicleTypeperhour.setTextColor(ContextCompat.getColor(ctx, R.color.DarkGray));

            cardViewovernight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    seletedParkingType = "Over Night";
                    vehicleTypeovernight.setTextColor(ContextCompat.getColor(ctx, R.color.DarkGray));
                    vehicleTypeperhour.setTextColor(ContextCompat.getColor(ctx, R.color.TextBlackColor));
                    cardViewovernight.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_gray));
                    cardViewperhour.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_light_golden));

                }
            });

            cardViewperhour.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    seletedParkingType = "Per Hour";
                    vehicleTypeperhour.setTextColor(ContextCompat.getColor(ctx, R.color.DarkGray));
                    vehicleTypeovernight.setTextColor(ContextCompat.getColor(ctx, R.color.TextBlackColor));
                    cardViewperhour.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_gray));
                    cardViewovernight.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_light_golden));
                }
            });
            //   overnightIMAGEVIEW.setColorFilter(Color.argb(0, 0, 0,0));

            overnightIMAGEVIEW.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    seletedParkingType = "Per Hour";
                    overnightIMAGEVIEW.setVisibility(View.GONE);
                    perHourIMAGEVIEWhite.setVisibility(View.VISIBLE);
                    getprice(vehiclEtype, seletedParkingType);
                }
            });

            perHourIMAGEVIEWhite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    seletedParkingType = "Over Night";
                    overnightIMAGEVIEW.setVisibility(View.VISIBLE);
                    perHourIMAGEVIEWhite.setVisibility(View.GONE);
                    getprice(vehiclEtype, seletedParkingType);
                }
            });


            recyclerViewFilter.addOnItemTouchListener(
                    new SubItemRecyclerListener(getActivity(), new SubItemRecyclerListener.OnItemClickListener(
                    ) {
                        @SuppressLint("ClickableViewAccessibility")
                        @Override
                        public String onItemClick(View view, final int position) {
                         /*   InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(vehicleNumber, InputMethodManager.SHOW_IMPLICIT);
*/
                            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                            vehicleNumber.requestFocus();
                            //    vehicleNumber.setInputType(InputType.TYPE_CLASS_NUMBER);
                            vehicleNumber.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                            vehicleNumber.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
                            aSwitch.setChecked(false);

                            try {

                                vehicleImg.setVisibility(View.VISIBLE);

                                if (isNetworkConnected(context)) {
                                    vehicleCategoryRecyclerAdapterPojo.notifyItemChanged(vehicleCategoryRecyclerAdapterPojo.selected_position);
                                    vehicleCategoryRecyclerAdapterPojo.selected_position = position;
                                    vehicleCategoryRecyclerAdapterPojo.notifyItemChanged(vehicleCategoryRecyclerAdapterPojo.selected_position);
                                    vehiclEtype = VehicleList.get(position).toString();

                                    if (vehiclEtype.equals("Auto")) {
                                        vehicleImg.setImageResource(R.drawable.ic_tempo);
                                        MyLog.i(TAG + "vehiclEtype$$$$", vehiclEtype);
                                        getprice(vehiclEtype, seletedParkingType);
                                    } else if (vehiclEtype.equals("Bus")) {
                                        vehicleImg.setImageResource(R.drawable.ic_bus);
                                        getprice(vehiclEtype, seletedParkingType);
                                    } else if (vehiclEtype.equals("Car")) {
                                        vehicleImg.setImageResource(R.drawable.ic_car);
                                        MyLog.i(TAG + "vehiclEtype$$$$", vehiclEtype);
                                        getprice(vehiclEtype, seletedParkingType);
                                    } else if (vehiclEtype.equals("Cycle")) {
                                        vehicleImg.setImageResource(R.drawable.ic_cycle);
                                        getprice(vehiclEtype, seletedParkingType);
                                    } else if (vehiclEtype.equals("Mini Bus")) {
                                        vehicleImg.setImageResource(R.drawable.ic_bus);
                                        getprice(vehiclEtype, seletedParkingType);
                                    } else if (vehiclEtype.equals("SUV")) {
                                        vehicleImg.setImageResource(R.drawable.ic_car);
                                        getprice(vehiclEtype, seletedParkingType);
                                    } else if (vehiclEtype.equals("Tempo")) {
                                        vehicleImg.setImageResource(R.drawable.ic_tempo);
                                        getprice(vehiclEtype, seletedParkingType);
                                    } else if (vehiclEtype.equals("2 Wheeler")) {
                                        vehicleImg.setImageResource(R.drawable.ic_bike);
                                        getprice(vehiclEtype, seletedParkingType);
                                    }
                                } else {
                                    vehicleCategoryRecyclerAdapter.notifyItemChanged(vehicleCategoryRecyclerAdapter.selected_position);
                                    vehicleCategoryRecyclerAdapter.selected_position = position;
                                    vehicleCategoryRecyclerAdapter.notifyItemChanged(vehicleCategoryRecyclerAdapter.selected_position);
                                    vehiclEtype = VehicleList2.get(position).toString();
                                    MyLog.i(TAG + "SvehiclEtypeoffline@@@@@", vehiclEtype);

                                    if (vehiclEtype.equals("Auto")) {
                                        vehicleImg.setImageResource(R.drawable.ic_tempo);
                                        MyLog.i(TAG + "vehiclEtype$$$$", vehiclEtype);
                                        getprice(vehiclEtype, seletedParkingType);
                                    } else if (vehiclEtype.equals("Bus")) {
                                        vehicleImg.setImageResource(R.drawable.ic_bus);
                                        getprice(vehiclEtype, seletedParkingType);
                                    } else if (vehiclEtype.equals("Car")) {
                                        vehicleImg.setImageResource(R.drawable.ic_car);
                                        MyLog.i(TAG + "vehiclEtype$$$$", vehiclEtype);
                                        getprice(vehiclEtype, seletedParkingType);
                                    } else if (vehiclEtype.equals("Cycle")) {
                                        vehicleImg.setImageResource(R.drawable.ic_cycle);
                                        getprice(vehiclEtype, seletedParkingType);
                                    } else if (vehiclEtype.equals("Mini Bus")) {
                                        vehicleImg.setImageResource(R.drawable.ic_bus);
                                        getprice(vehiclEtype, seletedParkingType);
                                    } else if (vehiclEtype.equals("SUV")) {
                                        vehicleImg.setImageResource(R.drawable.ic_car);
                                        getprice(vehiclEtype, seletedParkingType);
                                    } else if (vehiclEtype.equals("Tempo")) {
                                        vehicleImg.setImageResource(R.drawable.ic_tempo);
                                        getprice(vehiclEtype, seletedParkingType);
                                    } else if (vehiclEtype.equals("2 Wheeler")) {
                                        vehicleImg.setImageResource(R.drawable.ic_bike);
                                        getprice(vehiclEtype, seletedParkingType);
                                    }
                                }


                                MyLog.i(TAG + "VehicleList@@@@@", VehicleList.toString());
                                MyLog.i(TAG + "VehicleList2@@@@@", VehicleList2.toString());

                           /*     if (VehicleList.get(position).toString().equals("Auto") && VehicleList2.get(position).toString().equals("Auto")) {
                                    vehicleImg.setImageResource(R.drawable.ic_tempo);
                                    MyLog.i(TAG + "vehiclEtype$$$$", vehiclEtype);
                                    vehiclEtype = VehicleList.get(position).toString();
                                    vehiclEtype = VehicleList2.get(position).toString();
                                    getprice(vehiclEtype, seletedParkingType);
                                } else if (VehicleList.get(position).toString().equals("Bus") && VehicleList2.get(position).toString().equals("Bus")) {
                                    vehicleImg.setImageResource(R.drawable.ic_bus);
                                    vehiclEtype = VehicleList.get(position).toString();
                                    vehiclEtype = VehicleList2.get(position).toString();
                                    getprice(vehiclEtype, seletedParkingType);
                                } else if (VehicleList.get(position).toString().equals("Car") || VehicleList2.get(position).toString().equals("Car")) {
                                    vehicleImg.setImageResource(R.drawable.ic_car);
                                    vehiclEtype = VehicleList.get(position).toString();
                                    vehiclEtype = VehicleList2.get(position).toString();
                                    MyLog.i(TAG + "vehiclEtype$$$$", vehiclEtype);
                                    getprice(vehiclEtype, seletedParkingType);
                                } else if (VehicleList.get(position).toString().equals("Cycle") || VehicleList2.get(position).toString().equals("Cycle")) {
                                    vehicleImg.setImageResource(R.drawable.ic_cycle);
                                    vehiclEtype = VehicleList.get(position).toString();
                                    vehiclEtype = VehicleList2.get(position).toString();
                                    getprice(vehiclEtype, seletedParkingType);
                                } else if (VehicleList.get(position).toString().equals("Mini Bus") || VehicleList2.get(position).toString().equals("Mini Bus")) {
                                    vehicleImg.setImageResource(R.drawable.ic_bus);
                                    vehiclEtype = VehicleList.get(position).toString();
                                    vehiclEtype = VehicleList2.get(position).toString();
                                    getprice(vehiclEtype, seletedParkingType);
                                } else if (VehicleList.get(position).toString().equals("SUV") || VehicleList2.get(position).toString().equals("SUV")) {
                                    vehicleImg.setImageResource(R.drawable.ic_car);
                                    vehiclEtype = VehicleList.get(position).toString();
                                    vehiclEtype = VehicleList2.get(position).toString();
                                    getprice(vehiclEtype, seletedParkingType);
                                } else if (VehicleList.get(position).toString().equals("Tempo") || VehicleList2.get(position).toString().equals("Tempo")) {
                                    vehicleImg.setImageResource(R.drawable.ic_tempo);
                                    vehiclEtype = VehicleList.get(position).toString();
                                    vehiclEtype = VehicleList2.get(position).toString();
                                    getprice(vehiclEtype, seletedParkingType);
                                } else if (VehicleList.get(position).toString().equals("2 Wheeler") || VehicleList2.get(position).toString().equals("2 Wheeler")) {
                                    vehicleImg.setImageResource(R.drawable.ic_bike);
                                    vehiclEtype = VehicleList.get(position).toString();
                                    vehiclEtype = VehicleList2.get(position).toString();
                                    getprice(vehiclEtype, seletedParkingType);
                                }*/
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return null;
                        }
                    })
            );

            addparkingTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    //       closeKeyboard();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            vehicleImg.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    int type = vehicleNumber.getInputType();

                    if (type == (InputType.TYPE_CLASS_PHONE)) {
                        vehicleNumber.setInputType(InputType.TYPE_CLASS_TEXT);
                        //     vehicleNumber.setImeOptions(EditorInfo.TYPE_CLASS_TEXT);
                        MyLog.i("DDd", "TYPE_CLASS_PHONE");
                    } else if (type == (InputType.TYPE_CLASS_TEXT)) {
                        vehicleNumber.setInputType(InputType.TYPE_CLASS_PHONE);
                        MyLog.i("TYPE_CLASS_TEXT", "DDd");
                        //   vehicleNumber.setImeOptions(EditorInfo.TYPE_CLASS_PHONE);
                    }
                    //      vehicleNumber.setInputType(InputType.TYPE_CLASS_TEXT);
                }
            });

            capturEcarImageVIEW.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickType = "vehicle";
                    clicked = true;
                    selectImage();
                }
            });

            ownerIDLinearlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickType = "ID";
                    clicked = true;
                    selectOwnerIDImage();
                }
            });

            addparkingTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        seletedParkingType = addparkingTypeSpinner.getSelectedItem().toString();
                        MyLog.d("s%%%%eletedParkingType", seletedParkingType);
                        if (seletedParkingType.equalsIgnoreCase("Select Parking Type")) {
                            seletedParkingType = "";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            submitBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        vehicleNumberSTRING = vehicleNumber.getText().toString().toUpperCase();
                        Log.i("vehicleNumberSTRING", vehicleNumberSTRING);
                        vehicleOwnerSTRING = vehicleOwner.getText().toString();
                        mobileSTRING = mobile.getText().toString();
                        if (!vehicleNumberSTRING.isEmpty() && !vehiclEtype.isEmpty() && !seletedParkingType.isEmpty()) {
                            submitBTN.setEnabled(false);
                            submitBTN.setClickable(false);
                            //             sendParkingData();
                            getdate();
                            getsrno();
                            iPaddress = SharedPrefUtil.getIpaddress(ctx);
                            saveparkingDb();
                        } else if (vehicleNumberSTRING.isEmpty()) {
                            vehicleNumber.requestFocus();
                            vehicleNumber.setError("Enter Vehicle Number");
                        } else if (vehiclEtype.equalsIgnoreCase("")) {
                            Toast.makeText(getActivity(), "Select Vehicle Type", Toast.LENGTH_SHORT).show();

                        } else if (seletedParkingType.equalsIgnoreCase("")) {
                            Toast.makeText(getActivity(), "Select Parking Type", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public static void getVehicleType() {
        try {
            //         progressBarFilter.setVisibility(View.VISIBLE);
            IOUtils ioUtils = new IOUtils();
            String Link = Constants.vehicleMasterList;
            MyLog.i(TAG + " url@@@@@getVehicleType@", Link);

            ioUtils.getCommonStringRequest(Constants.GET, TAG, context, Link, new IOUtils.VolleyCallback() {
                @Override
                public void onSuccess(String result) {

                    try {
                        MyLog.i(TAG + "getVehicleTypeList#####", result);
                        //                   progressBarFilter.setVisibility(View.GONE);

                        JSONObject kkk = new JSONObject(result);
                        SharedPrefUtil.setJson(context, String.valueOf(kkk));

                        JSONArray resultARRAY = kkk.getJSONArray("data");
                        VehicleList = new ArrayList<>();
                        for (int i = 0; i < resultARRAY.length(); i++) {
                            JSONObject jsonObject = resultARRAY.getJSONObject(i);
                            String category = jsonObject.getString("type");
                            String perHou = jsonObject.getString("perHourCharge");
                            String overNigh = jsonObject.getString("overNightCharges");
                            billingDataList.setType(category);
                            billingDataList.setOverNightCharges(overNigh);
                            billingDataList.setPerHourCharge(perHou);
                            VehicleList.add(category);
                            billingDatabase.billingDao().addData(billingDataList);
                            MyLog.i(TAG + "billingDataList#####", billingDataList.toString());
                        }

                        vehicleCategoryRecyclerAdapterPojo.selected_position = -1;
                        vehicleCategoryRecyclerAdapterPojo =
                                new VehicleCategoryRecyclerAdapterPojo(VehicleList, context);
                        recyclerViewFilter.setAdapter(vehicleCategoryRecyclerAdapterPojo);
                        vehicleCategoryRecyclerAdapterPojo.notifyDataSetChanged();

                    } catch (Exception e) {
                        e.printStackTrace();
                        //              progressBarFilter.setVisibility(View.GONE);
                    }
                }
            }, new IOUtils.VolleyFailureCallback() {
                @Override
                public void onFailure(String result) {

                    try {

                        MyLog.i("ON_ERROR########", result);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
//            progressBarFilter.setVisibility(View.GONE);
        }
    }

  /*    private void sendParkingData() {
        String urlcategory = Constants.parkingIn;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("vehicleNumber", vehicleNumberSTRING.toUpperCase());
            jsonObject.put("parkingType", seletedParkingType);
            jsonObject.put("mobile", mobileSTRING);
            jsonObject.put("vehicleOwner", vehicleOwnerSTRING);
            jsonObject.put("vehicleType", vehiclEtype);
            jsonObject.put("image", uploadgalleryurl);
            jsonObject.put("ownerId", ownerIDurl);
            jsonObject.put("lat", latString);
            jsonObject.put("lon", longString);
            MyLog.d("creayedOBject@@@@@@@@", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ioUtils.sendCommonJSONObjectRequest(Constants.POST, TAG, getActivity(), urlcategory, jsonObject, new IOUtils.VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                MyLog.d("sendCommonJSONObjectRequest@@@@@@", result);
                JSONObject jsonObjectRes = null;
                try {
                    submitBTN.setEnabled(true);
                    submitBTN.setClickable(true);

                    clearText();
                    jsonObjectRes = new JSONObject(result);

                    JSONObject jsonObject1 = jsonObjectRes.getJSONObject("data");
                    MyLog.i(TAG + "jsonObject1", jsonObject1.toString());
                    parkInTM = jsonObject1.getString("parkInTM");
                    inDay = jsonObject1.getString("inDay");
                    parkingInDt = jsonObject1.getString("parkingInDt");
                    srNo = jsonObject1.getString("srno");
                    vehicleNumber_Print = jsonObject1.getString("vehicleNumber");
                    headerDate = jsonObject1.getString("headerDate");
                    parkingType = jsonObject1.getString("parkingType");

                    String messeage = jsonObjectRes.getString("message");
                    String status = jsonObjectRes.getString("status");

                    if (status.equalsIgnoreCase("true")) {
                        MainActivity.tourPager.setCurrentItem(1);
                        //         ViewParkingVehiclesFragment.getParkingList("", "");
                        Toast.makeText(getContext(), messeage, Toast.LENGTH_SHORT).show();
                        if (parkingType.equalsIgnoreCase("Over Night")) {
                            printRecieptDataOverNight();
                        } else {
                            printRecieptData();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new IOUtils.VolleyFailureCallback() {
            @Override
            public void onFailure(String result) {

            }
        });

    }*/


    private void saveparkingDb() {
        try {
            String flag = "no";
            ParkingList parkingList = new ParkingList();
            parkingList.setVehicleOwner(vehicleOwnerSTRING);
            parkingList.setVehicleType(vehiclEtype);
            parkingList.setMobile(mobileSTRING);
            parkingList.setParkingType(seletedParkingType);
            parkingList.setVehicleNumber(vehicleNumberSTRING);
            parkingList.setMobParkInDt(indateandtime);
            parkingList.setParkingInHrMin(currentTime);
            parkingList.setExitdone(flag);
            parkingList.setCopytoserver(flag);
            parkingList.setDeleted(flag);
            parkingList.setAddoncharges(finaladdcharges);
            parkingList.setPrice(price);

            if (clickType.equalsIgnoreCase("vehicle")) {
                //     uploadgalleryurl = url;
                parkingList.setImage(imageFilePath);
                parkingList.setImageFilePathGallery(imageFilePathGallery);
            } else if (clickType.equalsIgnoreCase("ID")) {
                //      ownerIDurl = url;
                parkingList.setOwneridimage(imageFilePathID);
            }
            parkingList.setSrno(String.valueOf(stringBuilder));
            MyLog.d("serialno@@@addd", String.valueOf(stringBuilder));

            AddVehicleFragment.myDatabase.parkingDao().addData(parkingList);
            MainActivity.tourPager.setCurrentItem(1);
            ViewParkingVehiclesFragment.getDatafromdatabase();
            submitBTN.setEnabled(true);
            submitBTN.setClickable(true);
            if (seletedParkingType.equalsIgnoreCase("Over Night")) {
                MyLog.d("Over Night", "Over Night");
                printRecieptDataOverNight();
                clearText();

            } else {
                printRecieptData();
                clearText();
                MyLog.d("PER HOUR", "PER HOUR");
            }
            clearText();
            MyLog.d("ListofDATRABASE!!!!!", parkingList.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearText() {
        try {
            clickType = "";
            vehicleNumberSTRING = "";
            vehicleOwnerSTRING = "";
            mobileSTRING = "";
            vehicleNumber.setText("");
            vehicleOwner.setText("");
            mobile.setText("");
            vehicleNumberimageVIEW.setImageResource(R.drawable.ic_camera);
            uploadgalleryurl = "";
            ownerIDurl = "";
            overnightIMAGEVIEW.setVisibility(View.GONE);
            perHourIMAGEVIEWhite.setVisibility(View.VISIBLE);
            ownerIDImg.setImageResource(R.drawable.ic_camera);
            seletedParkingType = "Per Hour";
            cardViewperhour.setBackground(ContextCompat.getDrawable(ctx, R.drawable.rounded_btn_gray));
            vehicleTypeperhour.setTextColor(ContextCompat.getColor(ctx, R.color.DarkGray));
            captureTXT.setText("Capture Image");
            captureTXT.setTextColor(ContextCompat.getColor(getActivity(), R.color.Gray));
            aSwitch.setChecked(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

            if (imm.isAcceptingText()) {
                MyLog.i(TAG + "SHOW", "@@@@@");
            } else {
                MyLog.i(TAG + "HIDE", "@@@@@");
            }
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

/*
    private void selectImage() {
        try {
            imageFilePath = "";

            try {

                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());

                filename = "IMG_" + timeStamp + ".jpg";
                File sd = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Vehicle-Photos");
                MyLog.e("sd", sd.toString());
                MyLog.e("filename", filename.toString());

                if (!sd.exists()) {
                    if (!sd.mkdirs()) {
                        MyLog.e("Error :: ", "Problem creating Image folder");
                    }
                }

                dest = new File(sd, filename);

                photoURI = FileProvider.getUriForFile(getActivity(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        dest);

                imageFilePath = dest.getPath();
                imageFilePathGallery = dest.getAbsolutePath();
                MyLog.e("imageFilePath", imageFilePath.toString());
                MyLog.e("imageFilePathGallery", imageFilePathGallery.toString());
                MyLog.e("photoURI", photoURI.toString());
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

                startActivityForResult(intent, 7);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

*/

    private void selectImage() {

        try {

            imageFilePath = "";

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());

            filename = "IMG_" + timeStamp + ".jpg";
            File sd = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "TeamST_Photos");

            if (!sd.exists()) {
                if (!sd.mkdirs()) {
                    MyLog.e("Error :: ", "Problem creating Image folder");
                }
            }

            dest = new File(sd, filename);

            Uri photoURI = FileProvider.getUriForFile(getActivity(),
                    "in.sukhothai.parkingmanagementapp" + ".provider",
                    dest);

            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

            startActivityForResult(intent, 7);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void selectOwnerIDImage() {

        try {

            imageFilePath = "";

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());

            filename = "IMG_" + timeStamp + ".jpg";
            File sd = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "TeamST_Photos");

            if (!sd.exists()) {
                if (!sd.mkdirs()) {
                    MyLog.e("Error :: ", "Problem creating Image folder");
                }
            }

            dest = new File(sd, filename);

            Uri photoURI = FileProvider.getUriForFile(getActivity(),
                    "in.sukhothai.parkingmanagementapp" + ".provider",
                    dest);

            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

            startActivityForResult(intent, 7);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // TODO SETIMAGE IN IMAGEVIEW working code
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        try {

            if (resultCode == RESULT_OK) {

                if (requestCode == 7) {

                    try {

                        //       detectTextFromImage();

                        MyLog.i("OriginalImage######", getReadableFileSize(dest.length()));

                        File compressedFile = new Compressor(getActivity()).compressToFile(dest);

                        dest = compressedFile;

                        Log.i("Compress######", getReadableFileSize(dest.length()));

                        imageFilePath = dest.getPath();
                        Log.i("imageFilePath######", imageFilePath.toString());
                        final Bitmap bitmap;
                        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                        bitmap = BitmapFactory.decodeFile(dest.getAbsolutePath(), bitmapOptions);

                        MyLog.d(TAG + "Bitmap#####", bitmap.toString());
                        vehicleNumberimageVIEW.setImageBitmap(bitmap);
                        //    new UploadImageFileToServer().execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                if (requestCode == 8) {

                    try {


                        MyLog.i("OriginalImage######", getReadableFileSize(dest.length()));

                        File compressedFile = new Compressor(getActivity()).compressToFile(dest);

                        dest = compressedFile;

                        Log.i("Compress######", getReadableFileSize(dest.length()));

                        imageFilePathID = dest.getPath();

                        Log.i("imageIDIDIDPath######", imageFilePathID.toString());
                        final Bitmap bitmap;
                        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                        bitmap = BitmapFactory.decodeFile(dest.getAbsolutePath(), bitmapOptions);

                        MyLog.d(TAG + "Bitmap#####", bitmap.toString());
                        ownerIDImg.setImageBitmap(bitmap);
                        //     new UploadImageFileToServer().execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } else if (resultCode == RESULT_CANCELED) {

                //Toast.makeText(getApplicationContext(), "cancelled!", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(getActivity(), "Failed Try Again", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void NetworkOpen() {

    }

    @Override
    public void NetworkClose() {

    }

    // TODO SETIMAGE IN IMAGEVIEW working code

    class UploadImageFileToServer extends AsyncTask<Void, Void, String> {

        private String webAddressToPost = Constants.getAwsUploadLink;
        /*        private ProgressDialog dialog = new ProgressDialog(getActivity());*/

        @Override
        protected void onPreExecute() {

            try {

         /*       dialog.setMessage("Uploading...");
                dialog.setCancelable(false);
                dialog.show();*/
                Log.i("Pre", "Execute");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                try {
                    sourceFile = new File(imageFilePath);
                    Log.i("doIn", "Background");
                    URL url = new URL(webAddressToPost);

                    Log.i(TAG + "url@@@", url.toString());
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Connection", "Keep-Alive");

                    conn.setChunkedStreamingMode(1024);

                    MultipartEntityBuilder builder = MultipartEntityBuilder.create();

                    builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
                    builder.addPart("image", new FileBody(sourceFile));
                    builder.addPart("path", new StringBody("Parking", ContentType.TEXT_PLAIN));

                    HttpEntity entity = builder.build();

                    conn.addRequestProperty("Content-length", entity.getContentLength() + "");
                    conn.addRequestProperty(entity.getContentType().getName(), entity.getContentType().getValue());

                    OutputStream os = conn.getOutputStream();
                    entity.writeTo(conn.getOutputStream());
                    os.close();
                    conn.connect();

                    if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        return readStream(conn.getInputStream());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //           dialog.dismiss();
                }

            } catch (OutOfMemoryError e) {
                e.printStackTrace();
                Log.i("Out Of", "Memory");
                //      dialog.dismiss();
            }

            return null;
        }

        private String readStream(InputStream in) {
            BufferedReader reader = null;
            StringBuilder builder = new StringBuilder();
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return builder.toString();
        }

        @Override
        protected void onPostExecute(String result) {

            //      dialog.dismiss();
            Log.i("ImagePathResult#######", result);

            try {

                JSONObject jsonObject = new JSONObject(result);
                String url = jsonObject.getString("url");
                if (clickType.equalsIgnoreCase("vehicle")) {
                    uploadgalleryurl = url;
                } else if (clickType.equalsIgnoreCase("ID")) {
                    ownerIDurl = url;
                    MyLog.d("ownerIDurl", ownerIDurl);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getReadableFileSize(long size) {

        if (size <= 0) {

            return "0";
        }

        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};

        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));

        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

/*    private void detectTextFromImage() {

        try {
            FirebaseVisionImage firebaseVisionImage = FirebaseVisionImage.fromFilePath(ctx, photoURI);
            FirebaseVisionTextDetector firebaseVisionText = FirebaseVision.getInstance().getVisionTextDetector();
            firebaseVisionText.detectInImage(firebaseVisionImage).addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
                @Override
                public void onSuccess(@NonNull FirebaseVisionText firebaseVisionText) {
                    //        displaytext(firebaseVisionText);
                    MyLog.d("firebaseVisionText", firebaseVisionText.toString());
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

/*    private void displaytext(FirebaseVisionText firebaseVisionText) {
        try {

            List<FirebaseVisionText.Block> blockList = firebaseVisionText.getBlocks();
            if (blockList.size() == 0) {
                //         Toast.makeText(ctx, "Text not found! Please Try Again !", Toast.LENGTH_SHORT).show();
                captureTXT.setText("Text not found! Please Try Again !");
                captureTXT.setTextColor(ContextCompat.getColor(getActivity(), R.color.DarkRed));
            } else {
                for (FirebaseVisionText.Block block : firebaseVisionText.getBlocks()) {
                    String text = block.getText();
                    vehicleNumber.setText(text);
                    captureTXT.setText("Capture Image");
                    captureTXT.setTextColor(ContextCompat.getColor(getActivity(), R.color.Gray));
                    MyLog.d("mytetx", text);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }*/

    public void printRecieptData() {
        MyLog.d("ipaddres@@print", iPaddress);
        try {
            try {
                new AsyncTcpEscPosPrint(ctx)
                        //172.16.5.16
                        .execute(this.getAsyncEscPosPrinterInTime(new TcpConnection(iPaddress, 9100)));
            } catch (NumberFormatException e) {
                new AlertDialog.Builder(ctx)
                        .setTitle("Invalid TCP port address")
                        .setMessage("Port field must be a number.")
                        .show();
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //16 is of gt
    //19 is of posiflex
    public void printRecieptDataOverNight() {
        try {
            //172.16.5.16
            MyLog.d("ipaddres@@print", iPaddress);
            try {
                new AsyncTcpEscPosPrint(ctx)
                        .execute(this.getAsyncEscPosPrinterInTimeOverNight(new TcpConnection(iPaddress, 9100)));
            } catch (NumberFormatException e) {
                new AlertDialog.Builder(ctx)
                        .setTitle("Invalid TCP port address")
                        .setMessage("Port field must be a number.")
                        .show();
                e.printStackTrace();
                MyLog.d("vehicleNumberSTRING", vehicleNumberSTRING);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public AsyncEscPosPrinter getAsyncEscPosPrinterInTime(DeviceConnection printerConnection) {
        AsyncEscPosPrinter printer = new AsyncEscPosPrinter(printerConnection, 180, 80f, 40);
        return printer.setTextToPrint(
                "[C]<img>" + PrinterTextParserImg.bitmapToHexadecimalString(printer, ctx.getResources().getDrawableForDensity(R.drawable.newlogo, DisplayMetrics.DENSITY_MEDIUM)) + "</img>\n\n" +
                        "[L]" + headerDate + "[R][C]SR# " + stringBuilder + "\n" +
                        "[L]-----------------------------------------------\n" +
                        "[L]<font size='tall'>Vehicle Number : " + vehicleNumberSTRING + "</font>\n" +
                        "[C]\n" +
                        "[L]Entry Time" + "   " + dayOfTheWeek + " " + today + "\n" +
                        "[L]<font size='tall'>" + currentTime + "</font>\n" +
                        "[L]-----------------------------------------------\n" +
                        "[L]<font size='normal'>Sukhothai India Pvt.Ltd. <b>GST # 27AAOCS623OP2ZM</b></font>\n" +
                        "[C]<b><font size='normal'>Terms of Parking</b>\n" +
                        "[L]* Parking will be at owner's risk.\n" +
                        "* No responsibility of any loss of valuables.\n" +
                        "* No responsibility for any vehicle damage by     other vehicles or by accident.\n" +
                        "* Any chargable timing above an hour will be      consider as next hour.\n" +
                        "* Operating Hours 7 AM - 11 PM'\n</font>\n" +
                        //    "[C]<barcode type='ean13' height='10'>831254784551</barcode>\n" +
                        "[C]<barcode type='ean13' height='10'>" + forprint + "</barcode>\n" +
                        "[C]<font size='normal'>Thank You</font>\n" +
                        "[C]\n" +
                        "[C]\n"


           /*  old printer            "[L]" + headerDate + "[R][C]SR# " + srNo + "\n" +
                        "[L]-----------------------------------------------\n" +
                        "[C]<font size='tall'>  Vehicle Number : " + vehicleNumber_Print + "</font>\n" +
                        "[C]\n" +
                        "[C]In Time\n" +
                        "[C]" + dayOfTheWeek + " " + today + "\n" +
                        "[C]<font size='tall'>" + currentTime + "</font>\n" +
                        "[L]-----------------------------------------------\n" +
                        "[L]<font size='normal'>Sukhothai India Pvt.Ltd. <b>GST # 27AAOCS623OP2ZM</b></font>\n" +
                        "[C]<b><font size='normal'>Terms of Parking</b>\n" +
                        "[L]* Parking will be at owner's risk.\n" +
                        "* No responsibility of any loss of valuables.\n" +
                        "* No responsibility for any vehicle damage by     other vehicles or by accident.\n" +
                        "* Any chargable timing above an hour will be      consider as next hour.\n" +
                        "* Operating Hours 7 AM - 11 PM'\n</font>\n" +
                        //          "[C]<barcode type='ean8' height='10' text='none'>" + "4512784" + "</barcode>\n" +
                        //          "[C]<barcode type='ean8' height='10' text='none'>" + srno + "</barcode>\n" +
                        //<barcode type='ean8'>4512784</barcode>
                        "[C]<font size='normal'>Thank You</font>\n" +
                        "[C]\n" +
                        "[C]\n"*/
        );
    }

    public AsyncEscPosPrinter getAsyncEscPosPrinterInTimeOverNight(DeviceConnection printerConnection) {
        AsyncEscPosPrinter printer = new AsyncEscPosPrinter(printerConnection, 203, 77f, 45);
        return printer.setTextToPrint(
                "[C]<img>" + PrinterTextParserImg.bitmapToHexadecimalString(printer, ctx.getResources().getDrawableForDensity(R.drawable.newlogo, DisplayMetrics.DENSITY_MEDIUM)) + "</img>\n\n" +
                        "[L]" + headerDate + "[R][C]SR# " + stringBuilder + "\n" +
                        "[L]-----------------------------------------------\n" +
                        "[L]<font size='tall'>Vehicle Number : " + vehicleNumberSTRING + "</font>\n" +
                        "[C]\n" +
                        "[L] Entry Time\n" +
                        "[L]" + dayOfTheWeek + " " + today + "\n" +
                        "[L]<font size='tall'>" + currentTime + "</font>\n" +
                        "[C](Night Parking)\n" +
                        "[L]-----------------------------------------------\n" +
                        "[L]<font size='normal'>Sukhothai India Pvt.Ltd. <b>GST # 27AAOCS623OP2ZM</b></font>\n" +
                        "[C]<b><font size='normal'>Terms of Parking</b>\n" +
                        "[L]* Parking will be at owner's risk.\n" +
                        "* No responsibility of any loss of valuables.\n" +
                        "* No responsibility for any vehicle damage by     other vehicles or by accident.\n" +
                        "* Any chargable timing above an hour will be      consider as next hour.\n" +
                        "* Operating Hours 7 AM - 11 PM'\n</font>\n" +
                        //   "[C]<barcode type='ean13' height='10'>831254784551</barcode>\n" +
                        "[C]<barcode type='ean13' height='10'>" + forprint + "</barcode>\n" +

                        //          "[C]<barcode type='ean8' height='10' text='none'>" + srno + "</barcode>\n" +
                        //<barcode type='ean8'>4512784</barcode>
                        "[C]<font size='normal'>Thank You</font>\n" +
                        "[C]\n" +
                        "[C]\n"
        );
    }

    public void getdate() {
        //for display date in 24hrs
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatter2 = new SimpleDateFormat("MMM d , yyyy");
        today = formatter.format(date);
        String today2 = formatter2.format(date);
        System.out.println("Today : " + today);
        System.out.println("Today : " + today2);
        Calendar c = Calendar.getInstance();
        int Hr24 = c.get(Calendar.HOUR_OF_DAY);
        int FS = c.get(Calendar.AM_PM);
        int Min = c.get(Calendar.MINUTE);
        SimpleDateFormat df = new SimpleDateFormat("HH:mm a");
        currentTime = df.format(date.getTime());
        Log.e("currentTime", currentTime.toString());
        indateandtime = today + "  " + currentTime;
        Log.e("indateandtimeAADD", String.valueOf(indateandtime));
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        dayOfTheWeek = sdf.format(d);
        fulldateformat = dayOfTheWeek + "," + today;
        headerDate = today2 + " " + currentTime;

        Date currentDate = new Date();
        Log.e("currentDate", currentDate.toString());
        //Sat Aug 14 20:09:09 GMT+05:30 2021
        currentdate = currentDate.toString();

        // Display a date in day, month, year format
        String dtStart = "2021-8-15T09:27:37Z";

        //Sun Aug 15 11:40:21 GMT+05:30 2021
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String dtStart1 = "15/08/2021  11:47 am";
        SimpleDateFormat formatternew = new SimpleDateFormat("dd/MM/yyyy HH:mm a");

        try {
            // Fri Oct 15 09:27:37 GMT+05:30 2010
            Date datew = format.parse(dtStart);
            Log.e("datew", datew.toString());
            Log.e("dtStart1", dtStart1.toString());
            Date datew1 = formatternew.parse(dtStart1);
            Log.e("datew1", datew1.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void getsrno() {
        try {
            finalSrno = SharedPrefUtil.getSRNO(ctx);
            MyLog.d("finalSrno$$", String.valueOf(finalSrno));

            if (finalSrno.equalsIgnoreCase("")) {
                finalSrno = "1";
                SharedPrefUtil.setSRNO(ctx, String.valueOf(finalSrno));
            } else {
                int temp = Integer.parseInt(finalSrno);
                MyLog.d("temp", String.valueOf(temp));
                temp += 1;
                //     temp++;
                MyLog.d("@@!temp", String.valueOf(temp));
                int finalsr = temp;
                MyLog.d("@@!finalsr", String.valueOf(finalsr));
                finalSrno = String.valueOf(finalsr);
                SharedPrefUtil.setSRNO(ctx, String.valueOf(finalsr));
            }
            int length = finalSrno.length();
            stringBuilder = new StringBuilder();
            for (int i = 0; i < 7 - length; i++) {
                stringBuilder.append("0");
            }

            stringBuilder.append(finalSrno);
            forprint = "000000" + stringBuilder;
            MyLog.e("stringBuilder", stringBuilder.toString());
            MyLog.e("forprint", forprint.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getprice(String vehicletpyee, String parkingtype) {
        MyLog.e("vehicletpye$$$", vehicletpyee);

       /* try {
            BillingDataBase billingdatabase = Room.databaseBuilder(getActivity(), BillingDataBase.class, "BillingCollection").allowMainThreadQueries().build();
            //  myDatabase.parkingDao().updateDelete(deleted, String.valueOf(md.getId()));
            class GetData extends AsyncTask<Void, Void, List<BillingDaoList>> {
                @Override
                protected List<BillingDaoList> doInBackground(Void... voids) {
                    billingdaolist = billingdatabase.billingDao().getBillingData();
                    MyLog.d("BillingDaoList", billingdaolist.toString());
                    return billingdaolist;
                }

                @SuppressLint("DefaultLocale")
                @Override
                protected void onPostExecute(List<BillingDaoList> myDataList) {
                    try {
                        Log.i(TAG + "BillingDaoList@@@", myDataList.toString());
                        //    String  ipaddress = myDataList.get(0).getIpaddress();

                        for (int q = 0; q < billingdaolist.size(); q++) {

                            //                  iPaddress = billingdaolist.get(0).getIpaddress().toString();
                            getVehicleType = billingdaolist.get(q).getType();
                            String Charges = billingdaolist.get(q).getAddonCharges();

                   *//*         if (getVehicleType.equalsIgnoreCase(vehiclEtype)) {
                                addCharges = Charges;
                                Toast.makeText(getActivity(), vehiclEtype, Toast.LENGTH_SHORT).show();
                            }*//*

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    super.onPostExecute(myDataList);
                }
            }
            GetData gd = new GetData();
            gd.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }*/


        JSONArray resultARRAY = null;
        try {
            mainobjectVehicleMaster = SharedPrefUtil.getJson(ctx);
            MyLog.e("mainobjectVehicleMaster$$$", mainobjectVehicleMaster);
            JSONObject ff = new JSONObject(mainobjectVehicleMaster);
            resultARRAY = ff.getJSONArray("data");
            //    VehicleList = new ArrayList<>();
            for (int i = 0; i < resultARRAY.length(); i++) {
                JSONObject jsonObject = resultARRAY.getJSONObject(i);
                getVehicleType = jsonObject.getString("type");
                perHour = jsonObject.getString("perHourCharge");
                overNight = jsonObject.getString("overNightCharges");

                if (getVehicleType.equalsIgnoreCase(vehicletpyee)) {
                    if (parkingtype.equalsIgnoreCase("Over Night")) {
                        price = overNight;
                        Log.i("priceoverNight", price);

                    } else {
                        price = perHour;
                        Log.i("price", price);

                    }
                } else {
                    Log.i("not", "n");
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {


        //     Toast.makeText(getActivity(), "The Switch is " + (b ? "on" : "off"),
        //            Toast.LENGTH_SHORT).show();
        if (b) {
            //do stuff when Switch is ON
            finaladdcharges = addCharges;
            Toast.makeText(getActivity(), finaladdcharges, Toast.LENGTH_SHORT).show();
        } else {
            //do stuff when Switch if OFF

            finaladdcharges = "null";

        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        MenuInflater inflater1 = getActivity().getMenuInflater();
        inflater1.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                showDialogBox(getActivity());
                break;
        }
        return true;
    }

    public void showDialogBox(Context context) {

        try {

            dialogTherap = new Dialog(context);
            dialogTherap.setContentView(R.layout.ipdialogbox);
            dialogTherap.setCancelable(false);
            closeHolderIMG = (ImageView) dialogTherap.findViewById(R.id.closeHolderIMG);
            vehicleNumberET = (TextInputEditText) dialogTherap.findViewById(R.id.vehicleNumberET);
            searchBTN = (FancyButton) dialogTherap.findViewById(R.id.searchBTN);
            //   billingDatabase.parkingDao().updateParkingList(String.valueOf(diffHours), headerDateAdapter, todayout, billAmount, String.valueOf(diffDays), String.valueOf(Fcgst), String.valueOf(Fsgst), copytoserver, exitdone, deleted, String.valueOf(md.getId()));
            // Get Current Date
            final Calendar c = Calendar.getInstance();
            c.setTimeZone(TimeZone.getTimeZone("UTC"));
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);
            closeHolderIMG.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        dialogTherap.dismiss();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            searchBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        dialogTherap.dismiss();
                        iPaddresS = vehicleNumberET.getText().toString();

                        SharedPrefUtil.setIpaddress(getActivity(), String.valueOf(iPaddresS));

                        //   Toast.makeText(BillingActivity.this, iPaddresS, Toast.LENGTH_SHORT).show();
                        //       getParkingList(vehicleNo, selectedDate);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialogTherap.getWindow();
            lp.copyFrom(window.getAttributes());

            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
            dialogTherap.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialogTherap.show();

            dialogTherap.setOnKeyListener(new Dialog.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                        MyLog.i("@@@@@@@BACK", "PRESSED");
                        dialogTherap.dismiss();
                        return true;
                    }
                    return false;
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void setGetVehicleTypeOffline() {
        try {
            JSONArray resultARRAY = null;
            try {
//                progressBarFilter.setVisibility(View.GONE);
                mainobjectVehicleMaster = SharedPrefUtil.getJson(context);
                MyLog.e("mainobjectOFFLINE$$", mainobjectVehicleMaster);
                JSONObject ff = new JSONObject(mainobjectVehicleMaster);
                resultARRAY = ff.getJSONArray("data");
                VehicleList2 = new ArrayList<>();
                for (int i = 0; i < resultARRAY.length(); i++) {
                    JSONObject jsonObject = resultARRAY.getJSONObject(i);
                    String getVehicleType = jsonObject.getString("type");
                    VehicleList2.add(getVehicleType);
                }
                vehicleCategoryRecyclerAdapter.selected_position = -1;
                vehicleCategoryRecyclerAdapter =
                        new VehicleCategoryRecyclerAdapter(VehicleList2, context);
                recyclerViewFilter.setAdapter(vehicleCategoryRecyclerAdapter);
                vehicleCategoryRecyclerAdapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}

