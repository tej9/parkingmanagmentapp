package in.sukhothai.parkingmanagementapp.POJO;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import in.sukhothai.parkingmanagementapp.RoomDataBase.Converter.Converter;


public class BillingVehiclePojo {


    public void setStatus(boolean status) {
        this.status = status;
    }

    @SerializedName("data")
    private List<DataItem> data;

    @SerializedName("status")
    private boolean status;


    public List<DataItem> getData() {
        return data;
    }

    public void setData(List<DataItem> data) {
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }
}