package in.sukhothai.parkingmanagementapp.RoomDataBase;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

//172.16.5.19

@Entity(tableName = "BillingVehiclePojo")
public class BillingDaoList {


    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "type")
    private String type;

    @ColumnInfo(name = "perHourCharge")
    private String perHourCharge;

    @ColumnInfo(name = "overNightCharges")
    private String overNightCharges;


    @ColumnInfo(name = "ipaddress")
    private String ipaddress;

    @ColumnInfo(name = "addonCharges")
    private String addonCharges;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPerHourCharge() {
        return perHourCharge;
    }

    public void setPerHourCharge(String perHourCharge) {
        this.perHourCharge = perHourCharge;
    }

    public String getOverNightCharges() {
        return overNightCharges;
    }

    public void setOverNightCharges(String overNightCharges) {
        this.overNightCharges = overNightCharges;
    }

    public String getIpaddress() {
        return ipaddress;
    }

    public void setIpaddress(String ipaddress) {
        this.ipaddress = ipaddress;
    }
    public String getAddonCharges() {
        return addonCharges;
    }

    public void setAddonCharges(String addonCharges) {
        this.addonCharges = addonCharges;
    }

    @Override
    public String toString() {
        return "BillingDaoList{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", perHourCharge='" + perHourCharge + '\'' +
                ", overNightCharges='" + overNightCharges + '\'' +
                ", ipaddress='" + ipaddress + '\'' +
                ", addonCharges='" + addonCharges + '\'' +
                '}';
    }
}
