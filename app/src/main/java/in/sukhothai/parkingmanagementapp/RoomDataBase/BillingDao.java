package in.sukhothai.parkingmanagementapp.RoomDataBase;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import in.sukhothai.parkingmanagementapp.POJO.BillingVehiclePojo;


@Dao
public interface BillingDao {

    @Insert
    public void addData(BillingDaoList billingDaoList);

    @Query("select * from BillingVehiclePojo")
    public List<BillingDaoList> getBillingData();

/*
    @Query("SELECT ipaddress FROM BillingVehiclePojo WHERE  id= :id")
    String getip( int id);
*/

//    @Query("SELECT * FROM movies WHERE id = :id")
  //  LiveData<Movies.Movie> getMovie(int id);*/
/*    @Query("UPDATE ParkingDb SET parkingHours =:parkingHours ,mobParkOutDt =:mobParkOutDt ,billAmount =:billAmount  WHERE id= id ")
    int updateParkingList(String parkingHours, String mobParkOutDt, String billAmount);

    @Query("UPDATE ParkingDb SET deleted =:deleted WHERE id= :id ")
    int updateDelete(String deleted ,String id );
    */

/*
    @Query("UPDATE BillingCollection SET ipaddress =:deleted WHERE id= :id ")
    int updateDelete(String deleted ,String id );
*/

}