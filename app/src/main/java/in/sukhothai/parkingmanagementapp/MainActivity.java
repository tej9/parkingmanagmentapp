package in.sukhothai.parkingmanagementapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseApp;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.provider.Settings;
import android.text.Html;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import id.zelory.compressor.Compressor;
import in.sukhothai.parkingmanagementapp.Utilities.CustomExpandableListAdapter;
import in.sukhothai.parkingmanagementapp.Utilities.IOUtils;
import in.sukhothai.parkingmanagementapp.Utilities.LocationUpdatesService;
import in.sukhothai.parkingmanagementapp.Utilities.MyLog;
import in.sukhothai.parkingmanagementapp.Utilities.SharedPrefUtil;
import in.sukhothai.parkingmanagementapp.Utilities.SlidingTab.SlidingTabLayout;
import in.sukhothai.parkingmanagementapp.network.NetworkUtils;
import in.sukhothai.parkingmanagementapp.network.NetworkUtilsReceiver;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, NetworkUtilsReceiver.NetworkResponseInt {
    private String TAG = this.getClass().getSimpleName();
    private RelativeLayout mainRelLayout;
    private SlidingTabLayout tourTabs;
    public static ViewPager tourPager;
    private NetworkUtilsReceiver networkUtilsReceiver;
    boolean doubleBackToExitPressedOnce = false;
    private ExpandableListView expListView;
    private ExpandableListAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {

            setContentView(R.layout.activity_main);
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            setTitle("Parking Management");

            MainActivity.this.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

            //TODO Register receiver
            networkUtilsReceiver = new NetworkUtilsReceiver(this);
            registerReceiver(networkUtilsReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            //TODO Check Run Time Permission
            if (Build.VERSION.SDK_INT >= 23) {
                TedPermission.with(MainActivity.this)
                        .setPermissionListener(permissionlistener)
                        .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE)
                        .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                        .check();
            } else {
                startLocationServices(MainActivity.this);
            }

            expListView = (ExpandableListView) findViewById(R.id.lvExp);


            mainRelLayout = (RelativeLayout) findViewById(R.id.mainRelLayout);
            tourTabs = (SlidingTabLayout) findViewById(R.id.tourTabSlider);
            tourPager = (ViewPager) findViewById(R.id.tourTabsPager);
            tourPager.setAdapter(new viewPagerAdapter(getSupportFragmentManager()));
            tourTabs.setViewPager(tourPager);
            tourTabs.setSelectedIndicatorColors(ContextCompat.getColor(MainActivity.this, R.color.bluegray100));
            tourPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    try {
                        if (position == 1) {
                            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                            inputMethodManager.hideSoftInputFromWindow(mainRelLayout.getWindowToken(), 0);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            tourTabs = (SlidingTabLayout) findViewById(R.id.tourTabSlider);
            tourPager = (ViewPager) findViewById(R.id.tourTabsPager);
            tourPager.setAdapter(new viewPagerAdapter(getSupportFragmentManager()));
            tourTabs.setViewPager(tourPager);
            tourTabs.setSelectedIndicatorColors(ContextCompat.getColor(MainActivity.this, R.color.bluegray100));

            tourPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {

                    try {

                        if (position == 1) {
                            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                            inputMethodManager.hideSoftInputFromWindow(mainRelLayout.getWindowToken(), 0);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            prepareListData();
            addDrawerEventListen();
            listAdapter = new CustomExpandableListAdapter(this, listDataHeader, listDataChild);
            expListView.setAdapter(listAdapter);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();
            //  toolbar.setNavigationIcon(R.drawable.service);
            toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));
            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(MainActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    class viewPagerAdapter extends FragmentPagerAdapter {
        String tabs[];


        public viewPagerAdapter(FragmentManager fm) {
            super(fm);
            tabs = getResources().getStringArray(R.array.addViewTabsName);
        }

        @Override
        public Fragment getItem(int position) {

            Fragment f = null;

            switch (position) {
                case 0: {
                    f = new AddVehicleFragment();

                    break;
                }
                case 1: {
                    f = new ViewParkingVehiclesFragment();
                    break;
                }
            }
            return f;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    PermissionListener permissionlistener = new PermissionListener() {
        @Override
        public void onPermissionGranted() {

            try {

                startLocationServices(MainActivity.this);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
            Toast.makeText(MainActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
        }
    };

    //TODO Location Services Enable
    public void startLocationServices(Context context) {

        try {

            final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

                IOUtils.buildAlertMessageNoGps(MainActivity.this);

            } else {

                if (!IOUtils.isServiceRunning(LocationUpdatesService.class, context)) {
                    // LOCATION SERVICE
                    startService(new Intent(context, LocationUpdatesService.class));
                    MyLog.e(TAG, "Location service is already running");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            switch (requestCode) {
                case 01:
                    switch (resultCode) {
                        case Activity.RESULT_OK:
                            // All required changes were successfully made
                            if (!IOUtils.isServiceRunning(LocationUpdatesService.class, MainActivity.this)) {
                                // LOCATION SERVICE
                                startService(new Intent(MainActivity.this, LocationUpdatesService.class));
                                MyLog.e(TAG, "Location service is already running");
                            }
                            break;
                        case Activity.RESULT_CANCELED:
                            // The user was asked to change settings, but chose not to
                            break;
                        default:
                            break;
                    }
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
            MyLog.i("Error", e.getMessage());
        }
    }

    //TODO Check Network Availability
    @Override
    public void NetworkOpen() {
        AddVehicleFragment.getVehicleType();

        try {
            Toast.makeText(MainActivity.this, "Device is Connected to Internet", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void NetworkClose() {
        Toast.makeText(MainActivity.this, "Device Lost Internet Conection", Toast.LENGTH_SHORT).show();
  //      AddVehicleFragment.getVehicleType();
//        AddVehicleFragment.progressBarFilter.setVisibility(View.GONE);
        AddVehicleFragment.setGetVehicleTypeOffline();

        try {

            if (!NetworkUtils.isNetworkConnectionOn(MainActivity.this)) {

                Snackbar snackbar = Snackbar
                        .make(mainRelLayout, getResources().getString(R.string.error_message_no_internet), Snackbar.LENGTH_LONG)
                        .setAction(getResources().getString(R.string.settings), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));

                            }
                        });

                snackbar.setActionTextColor(Color.RED);
                snackbar.show();
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isTimeAutomatic(Context c) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.Global.getInt(c.getContentResolver(), Settings.Global.AUTO_TIME, 0) == 1;
        } else {
            return android.provider.Settings.System.getInt(c.getContentResolver(), android.provider.Settings.System.AUTO_TIME, 0) == 1;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isTimeAutomatic(MainActivity.this)) {

        } else {
            startActivity(new Intent(android.provider.Settings.ACTION_DATE_SETTINGS));
            Toast.makeText(this, "Please Set Your Time and Date Settings to Auto", Toast.LENGTH_SHORT).show();
        }
    }

    //TODO Menu listing
    private void prepareListData() {

        try {
            listDataHeader = new ArrayList<String>();
            listDataChild = new HashMap<String, List<String>>();
            listDataHeader.add("Master List");
            /*listDataHeader.add("Common");
            listDataHeader.add("Feedback");
            listDataHeader.add("General");*/

            List<String> myModule = new ArrayList<String>();
            /*myModule.add("Update Receipt");
            myModule.add("Allocate Promo Card");*/
            myModule.add("Home");
            myModule.add("Status");
            myModule.add("Feedback");
            myModule.add("Admin");

            /*List<String> leads = new ArrayList<String>();
            leads.add("Sub menu");

            List<String> feedback = new ArrayList<String>();
            feedback.add("Sub menu");

            List<String> general = new ArrayList<String>();
            general.add("Sukho thai");

            // Header, Child data
            listDataChild.put(listDataHeader.get(0), common);
            listDataChild.put(listDataHeader.get(1), leads);
            listDataChild.put(listDataHeader.get(2), feedback);*/
            listDataChild.put(listDataHeader.get(0), myModule);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addDrawerEventListen() {
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                String childName = listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition).toString();
             //   Toast.makeText(MainActivity.this, childName, Toast.LENGTH_SHORT).show();
         /*       if (childName.equals("Admin")) {
                    Intent intent = new Intent(MainActivity.this, BillingActivity.class);
                    startActivity(intent);
                }*/
                return false;
            }
        });
    }
}
