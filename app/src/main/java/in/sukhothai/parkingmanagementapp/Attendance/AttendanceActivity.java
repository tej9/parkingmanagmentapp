package in.sukhothai.parkingmanagementapp.Attendance;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import in.sukhothai.parkingmanagementapp.R;
import in.sukhothai.parkingmanagementapp.Utilities.Constants;
import in.sukhothai.parkingmanagementapp.Utilities.IOUtils;
import in.sukhothai.parkingmanagementapp.Utilities.MyLog;
import mehdi.sakout.fancybuttons.FancyButton;

public class AttendanceActivity extends AppCompatActivity {
    private RecyclerView attendancerecylerview;
    Spinner clickaction;
    ImageView mobileHolder;
    static AttendanceAdapter attendanceAdapter;
    private LinearLayoutManager linearLayoutManager;
    public static Dialog dialogTherapy;
    private ImageView closeHolderIMG;
    private TextInputEditText vehicleNumberET;
    private LinearLayout dateHolder;
    private FancyButton dateFB, searchBTN, markattendanceBTN;
    private String selectedDate, food = "";
    private static TherepiestResponse therapistMainPOJO;
    static Gson gson;
    private int p;
    private String name, type ,today ,newDateStr;
    static List<String> ttendance;
    ArrayList<String> example;
    static String TAG = "Attendance";
    static JSONArray obj;
   static JSONObject mainobj;
    static JSONObject list1;
    private IOUtils ioUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);
        attendancerecylerview = findViewById(R.id.attendancerecylerview);
        markattendanceBTN = findViewById(R.id.markattendanceBTN);
        attendancerecylerview.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(AttendanceActivity.this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        attendancerecylerview.setLayoutManager(linearLayoutManager);
        setTitle("Attendance");
        gson = new Gson();
        obj = new JSONArray();
        getdate();
        ioUtils = new IOUtils();
        mainobj = new JSONObject();
        //  foodlist = Arrays.asList(new String[]{"SELECT", "PRESENT", "RECEPTION", "ABSENT"});

        getTherapistList();

        //   attendanceAdapter = new AttendanceAdapter(AttendanceActivity.this);
        markattendanceBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MyLog.e("mainobj", mainobj.toString());
                try {
                    mainobj.put("attendanceDate",newDateStr);
                    mainobj.put("branch","IN007");
                    mainobj.put("data",obj);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                sendParkingData(mainobj);
            }

        });
        attendancerecylerview.setAdapter(attendanceAdapter);

       /* attendancerecylerview.addOnItemTouchListener(
                new SubItemRecyclerListener(getApplicationContext(), new SubItemRecyclerListener.OnItemClickListener() {
                    @SuppressLint("ClickableViewAccessibility")
                    @Override
                    public String onItemClick(View view, final int position) {
                        try {
                            RecyclerView.ViewHolder holder = attendancerecylerview.findViewHolderForAdapterPosition(position);
                            clickaction = holder.itemView.findViewById(R.id.actionspinner);
                             mobileHolder = holder.itemView.findViewById(R.id.phone);
                             mobileHolder.setOnTouchListener(new View.OnTouchListener() {
                                @Override
                                public boolean onTouch(View v, MotionEvent event) {

                                    try {

                                        if (therapistMainPOJO.getData().get(position).getF21MOBILE() != null) {

                                            mobileNo = therapistMainPOJO.getData().get(position).getF21MOBILE().toString();

                                        } else {

                                            mobileNo = "";
                                        }

                                        clickToCall(mobileNo);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    return false;
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        return null;
                    }
                })
        );*/
    }

    public void showDialogBox(Context context) {

        try {

            dialogTherapy = new Dialog(context);
            dialogTherapy.setContentView(R.layout.searchvehicledialogbox);
            dialogTherapy.setCancelable(false);
            closeHolderIMG = (ImageView) dialogTherapy.findViewById(R.id.closeHolderIMG);
            vehicleNumberET = (TextInputEditText) dialogTherapy.findViewById(R.id.vehicleNumberET);
            dateHolder = (LinearLayout) dialogTherapy.findViewById(R.id.dateHolder);
            dateFB = (FancyButton) dialogTherapy.findViewById(R.id.dateFB);
            searchBTN = (FancyButton) dialogTherapy.findViewById(R.id.searchBTN);

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            c.setTimeZone(TimeZone.getTimeZone("UTC"));
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);

            final DatePickerDialog datePickerDialog = new DatePickerDialog(
                    AttendanceActivity.this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                    try {

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
                        selectedDate = format1.format(calendar.getTime());
                        dateFB.setText(selectedDate);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, mYear, mMonth, mDay);

            dateHolder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {

                        datePickerDialog.setTitle("Select date");
                        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                        datePickerDialog.show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            closeHolderIMG.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        dialogTherapy.dismiss();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            searchBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        dialogTherapy.dismiss();
                        String vehicleNo = vehicleNumberET.getText().toString();
                        //       getParkingList(vehicleNo, selectedDate);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialogTherapy.getWindow();
            lp.copyFrom(window.getAttributes());

            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
            dialogTherapy.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialogTherapy.show();

            dialogTherapy.setOnKeyListener(new Dialog.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                        MyLog.i("@@@@@@@BACK", "PRESSED");
                        dialogTherapy.dismiss();
                        return true;
                    }
                    return false;
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        MenuInflater inflater1 = AttendanceActivity.this.getMenuInflater();
        inflater1.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                showDialogBox(getActivity());
                break;
        }
        return true;
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                showDialogBox(AttendanceActivity.this);
                break;
        }
        return true;
    }

    public void getTherapistList() {

        try {

            //       progressBar.setVisibility(View.VISIBLE);

            IOUtils ioUtils = new IOUtils();

            //    String branchCode = SharedPrefUtil.getUserLogin(ctx).getData().getSTBRN();

            String jsonLink = "http://login.sukhothai.in/route/F21_STAF/getTherapistList/IN007";
//              Constants.getTherapistList + branchCode;

            MyLog.i(TAG + " url", jsonLink);

            ioUtils.getCommonStringRequest(Constants.GET, TAG, AttendanceActivity.this
                    , jsonLink, new IOUtils.VolleyCallback() {
                        @Override
                        public void onSuccess(String result) {

                            try {

                                MyLog.i("Response#####", result);
                                //  progressBar.setVisibility(View.GONE);
                                getTherapistListResponse(result);

                            } catch (Exception e) {
                                e.printStackTrace();
                                //   progressBar.setVisibility(View.GONE);
                            }
                        }
                    }, new IOUtils.VolleyFailureCallback() {
                        @Override
                        public void onFailure(String result) {

                            try {

                                MyLog.i("ON_ERROR########", result);
                                JSONObject jsonObject1 = new JSONObject(result);

                                if (jsonObject1.has("message")) {

                                    String message = jsonObject1.getString("message");
                                    Toast.makeText(AttendanceActivity.this, message, Toast.LENGTH_SHORT).show();

                                } else {

                                    Toast.makeText(AttendanceActivity.this, "Error!!", Toast.LENGTH_SHORT).show();
                                }

                                //    progressBar.setVisibility(View.GONE);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

        } catch (Exception e) {
            e.printStackTrace();
            //     progressBar.setVisibility(View.GONE);
        }
    }

    public void getTherapistListResponse(String response) {

        try {

            JSONObject jsonObject = new JSONObject(response);

            if (jsonObject.has("success")) {

                String success = jsonObject.getString("success");

                if (success.equalsIgnoreCase("true")) {

                    therapistMainPOJO = gson.fromJson(response, TherepiestResponse.class);

                    attendanceAdapter = new AttendanceAdapter(therapistMainPOJO.getData(), AttendanceActivity.this);
                    attendancerecylerview.setAdapter(attendanceAdapter);
                    attendanceAdapter.notifyDataSetChanged();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            //     progressBar.setVisibility(View.GONE);
        }
    }


    public static void sendattendance(int position, String name, String type, String id, String desg) {
        list1 = new JSONObject();
        ttendance = new ArrayList<>();
        ttendance.add(String.valueOf(position));
        ttendance.add(name);
        ttendance.add(type);
        ttendance.add(id);
        ttendance.add(desg);
        MyLog.i("ttendance", ttendance.toString());
        try {
            //     for (int i = 0; i <= therapistMainPOJO.getLength(); i++) {
            try {
                // obj.remove(position);
                list1 = new JSONObject();
                list1.put("userId", id);
                if (type.equalsIgnoreCase("PRESENT")) {
                    list1.put("status", "P");
                } else if (type.equalsIgnoreCase("ABSENT")) {
                    list1.put("status", "A");
                } else if (type.equalsIgnoreCase("SELECT")) {
                    list1.put("status", "");
                }
                //       foodlist = Arrays.asList(new String[]{"SELECT", "PRESENT", "RECEPTION", "ABSENT"});

                obj.put(position, list1);
                String dddd = String.valueOf(obj.getString(position));
                MyLog.i("dddd", dddd.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
         //   mainobj.put(position, list1);
         //   MyLog.i("obj", obj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendParkingData(JSONObject jsonArray) {
        String urlcategory = "http://192.168.1.7:5002/route/attendance/saveStafAttdApp";
       // Constants.parkingIn;

        MyLog.d("JasonArray", jsonArray.toString());
        ioUtils.sendCommonJSONObjectRequest(Constants.POST, TAG, AttendanceActivity.this, urlcategory, jsonArray, new IOUtils.VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                MyLog.d("sendCommonJSONObjectRequest@@@@@@", result);
                JSONObject jsonObjectRes = null;
                try {
                    markattendanceBTN.setEnabled(true);
                    markattendanceBTN.setClickable(true);

                    //   clearText();
                    jsonObjectRes = new JSONObject(result);

                    String messeage = jsonObjectRes.getString("message");
                    String status = jsonObjectRes.getString("status");

                 /*   if (status.equalsIgnoreCase("true")) {
                        MainActivity.tourPager.setCurrentItem(1);
                        //         ViewParkingVehiclesFragment.getParkingList("", "");
                        Toast.makeText(getContext(), messeage, Toast.LENGTH_SHORT).show();
                        if (parkingType.equalsIgnoreCase("Over Night")) {
                            printRecieptDataOverNight();
                        } else {
                            printRecieptData();
                        }
                    }
*/
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new IOUtils.VolleyFailureCallback() {
            @Override
            public void onFailure(String result) {

            }
        });

    }

    public void getdate() {
        //for display date in 24hrs
        Date date = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatter2 = new SimpleDateFormat("MMM d , yyyy");
         today = formatter.format(date);
        today.replace("\"\"", "");
        Date currentDate = new Date();
        Log.e("currentDate", currentDate.toString());
        Log.e("today", today.toString());
    }
}