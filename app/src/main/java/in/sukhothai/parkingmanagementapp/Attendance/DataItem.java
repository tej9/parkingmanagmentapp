package in.sukhothai.parkingmanagementapp.Attendance;

import com.google.gson.annotations.SerializedName;

public class DataItem{

	@SerializedName("F21_SNAME")
	private String f21SNAME;

	@SerializedName("F21_SUKONM")
	private String f21SUKONM;

	@SerializedName("F21_DESIGN")
	private String f21DESIGN;

	@SerializedName("F21_IDNO")
	private int f21IDNO;

	@SerializedName("ERP_______")
	private String eRP;

	@SerializedName("F21_MOBILE")
	private String f21MOBILE;

	@SerializedName("F21_MF")
	private String f21MF;

	@SerializedName("IMG_PHOTO")
	private String iMGPHOTO;

	@SerializedName("F21_FNAME")
	private String f21FNAME;

	@SerializedName("_id")
	private String id;

	@SerializedName("F21_PRE")
	private String f21PRE;

	@SerializedName("ST_BRN")
	private String sTBRN;

	public String getF21SNAME(){
		return f21SNAME;
	}

	public String getF21SUKONM(){
		return f21SUKONM;
	}

	public String getF21DESIGN(){
		return f21DESIGN;
	}

	public int getF21IDNO(){
		return f21IDNO;
	}

	public String getERP(){
		return eRP;
	}

	public String getF21MOBILE(){
		return f21MOBILE;
	}

	public String getF21MF(){
		return f21MF;
	}

	public String getIMGPHOTO(){
		return iMGPHOTO;
	}

	public String getF21FNAME(){
		return f21FNAME;
	}

	public String getId(){
		return id;
	}

	public String getF21PRE(){
		return f21PRE;
	}

	public String getSTBRN(){
		return sTBRN;
	}
}