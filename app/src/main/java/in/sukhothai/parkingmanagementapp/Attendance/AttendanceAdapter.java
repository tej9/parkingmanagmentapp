package in.sukhothai.parkingmanagementapp.Attendance;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import in.sukhothai.parkingmanagementapp.POJO.VehicleCategoryRecyclerAdapter;
import in.sukhothai.parkingmanagementapp.R;
import in.sukhothai.parkingmanagementapp.Utilities.MyLog;

public class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.ViewHolder> {
    private List<DataItem> dataItemList;
    private Context context;
    private String mobileNo;
    static  JSONArray jsonArray ;
    String therapistFirstName, therapistDESIGN, therapistID, type = "";

    public AttendanceAdapter(List<DataItem> dataItemList, Context context) {
        this.dataItemList = dataItemList;
        this.context = context;
    }

    @NonNull
    @Override
    public AttendanceAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_attendance, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AttendanceAdapter.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        List<String> foodlist;

        foodlist = Arrays.asList(new String[]{"SELECT", "PRESENT", "RECEPTION", "ABSENT"});
        mobileNo = dataItemList.get(position).getF21MOBILE().toString();

        ArrayAdapter<String> relationspinner = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, foodlist);
        relationspinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        holder.actionspinner.setAdapter(relationspinner);
        relationspinner.notifyDataSetChanged();

        //holder.card_view.setVisibility(View.VISIBLE);

        if (dataItemList.get(position).getF21FNAME() != null) {

            therapistFirstName = dataItemList.get(position).getF21SUKONM();
            MyLog.i("name", therapistFirstName);
        } else {

            therapistFirstName = "";
        }

        holder.name.setText(therapistFirstName);

        if (dataItemList.get(position).getF21DESIGN() != null) {

            therapistDESIGN = dataItemList.get(position).getF21DESIGN();
            MyLog.i("name", therapistFirstName);
        } else {
            therapistDESIGN = "";
        }

        holder.desg.setText(therapistDESIGN);
        therapistID = String.valueOf(dataItemList.get(position).getF21IDNO());
        if (therapistID != null) {

            therapistID = String.valueOf(dataItemList.get(position).getF21IDNO());
            MyLog.i("name", therapistID);
        } else {
            therapistID = "";
        }

        holder.id.setText(therapistID);
        holder.phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    if (mobileNo != null) {

                        mobileNo = dataItemList.get(position).getF21MOBILE().toString();

                    } else {

                        mobileNo = "";
                    }

                    clickToCall(mobileNo);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        holder.actionspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                type = holder.actionspinner.getSelectedItem().toString();
                therapistFirstName = dataItemList.get(position).getF21FNAME().toString();
                therapistDESIGN = dataItemList.get(position).getF21DESIGN();
                therapistID = String.valueOf(dataItemList.get(position).getF21IDNO());

                //  therapistFirstName = dataItemList.get(position).getF21FNAME().toString();
                //     sendattendance(position,therapistFirstName,type);
                AttendanceActivity.sendattendance(position, therapistFirstName, type,therapistID,therapistDESIGN );

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        MyLog.d("sendattendance", "sendattendance");

        holder.id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String type = holder.actionspinner.getSelectedItem().toString();
                Toast.makeText(context, position + type, Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public int getItemCount() {
        return dataItemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        Spinner actionspinner;
        TextView id, name, desg;
        ImageView phone;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            actionspinner = itemView.findViewById(R.id.actionspinner);
            id = itemView.findViewById(R.id.attendanceID);
            name = itemView.findViewById(R.id.attendanceNAME);
            desg = itemView.findViewById(R.id.attendanceDESGINATION);
            phone = itemView.findViewById(R.id.phone);
        }
    }

    private void clickToCall(String mobileNo) {

        try {

            if (!mobileNo.equalsIgnoreCase("")) {

                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + mobileNo));
                context.startActivity(callIntent);

            } else {

                Toast.makeText(context, "mobile no not available", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
