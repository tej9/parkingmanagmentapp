package in.sukhothai.parkingmanagementapp.Attendance;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class TherepiestResponse{

	@SerializedName("data")
	private List<DataItem> data;

	@SerializedName("success")
	private boolean success;

	@SerializedName("length")
	private int length;

	@SerializedName("message")
	private String message;

	public List<DataItem> getData(){
		return data;
	}

	public boolean isSuccess(){
		return success;
	}

	public int getLength(){
		return length;
	}

	public String getMessage(){
		return message;
	}
}