package in.sukhothai.parkingmanagementapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.room.Room;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import in.sukhothai.parkingmanagementapp.RoomDataBase.BillingDaoList;
import in.sukhothai.parkingmanagementapp.RoomDataBase.BillingDataBase;
import in.sukhothai.parkingmanagementapp.RoomDataBase.ParkingDataBase;
import in.sukhothai.parkingmanagementapp.RoomDataBase.ParkingList;
import in.sukhothai.parkingmanagementapp.Utilities.MyLog;
import in.sukhothai.parkingmanagementapp.Utilities.SharedPrefUtil;
import mehdi.sakout.fancybuttons.FancyButton;

public class BillingActivity extends AppCompatActivity {
    EditText type, perHourCharge, overNightCharges, addonCharges;
    String typeS, perHourChargeS, overNightChargesS, iPaddresS, addonChargeS;
    Button submit;
    private static BillingDataBase billingDatabase;
    private static Dialog dialogTherap;
    private ImageView closeHolderIMG;
    private TextInputEditText vehicleNumberET;
    private LinearLayout dateHolder;
    private FancyButton dateFB, searchBTN;
    BillingDaoList billingDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing);
        billingDatabase = Room.databaseBuilder(BillingActivity.this, BillingDataBase.class, "BillingCollection").allowMainThreadQueries().build();
        setTitle("Add Vehicles");
        //     billingdatabase = Room.databaseBuilder(context, BillingDataBase.class, "BillingCollection").allowMainThreadQueries().build();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Parking Management");

        BillingActivity.this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        billingDataList = new BillingDaoList();
        overNightCharges = findViewById(R.id.overNightCharges);
        addonCharges = findViewById(R.id.addonCharges);
        //  iPaddress = findViewById(R.id.iPaddress);
        perHourCharge = findViewById(R.id.perHourCharge);
        submit = findViewById(R.id.submit);
        type = findViewById(R.id.type);
        //  String fff = billingDatabase.billingDao().getip(0);
        // String fff =    billingdatabase.billingDao().getip(0);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    perHourChargeS = perHourCharge.getText().toString().toUpperCase();
                    typeS = type.getText().toString().toUpperCase();
                    MyLog.d("typeS", typeS);

                    overNightChargesS = overNightCharges.getText().toString();
                    addonChargeS = addonCharges.getText().toString();
                    //         iPaddresS = iPaddress.getText().toString();
                    billingDataList.setPerHourCharge(perHourChargeS);
                    billingDataList.setType(typeS);
                    //     billingDataList.setIpaddress(iPaddresS);
                    billingDataList.setOverNightCharges(overNightChargesS);
                    billingDataList.setAddonCharges(addonChargeS);
                    //                 billingDatabase.billingDao().addData(billingDataList);
              /*  MainActivity.tourPager.setCurrentItem(1);
                ViewParkingVehiclesFragment.getDatafromdatabase();*/
                    type.setText("");
                    overNightCharges.setText("");
                    perHourCharge.setText("");
                    addonCharges.setText("");
                    MyLog.d("ListofDATRABASE!!!!!_ViewParkingVehiclesFragment", billingDataList.toString());
                    Intent intent = new Intent(BillingActivity.this, MainActivity.class);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                showDialogBox(BillingActivity.this);
                break;
        }
        return true;
    }
/*
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        MenuInflater inflater1 = getActivity().getMenuInflater();
        inflater1.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    public void showDialogBox(Context context) {

        try {

            dialogTherap = new Dialog(context);
            dialogTherap.setContentView(R.layout.ipdialogbox);
            dialogTherap.setCancelable(false);
            closeHolderIMG = (ImageView) dialogTherap.findViewById(R.id.closeHolderIMG);
            vehicleNumberET = (TextInputEditText) dialogTherap.findViewById(R.id.vehicleNumberET);
            searchBTN = (FancyButton) dialogTherap.findViewById(R.id.searchBTN);
            //   billingDatabase.parkingDao().updateParkingList(String.valueOf(diffHours), headerDateAdapter, todayout, billAmount, String.valueOf(diffDays), String.valueOf(Fcgst), String.valueOf(Fsgst), copytoserver, exitdone, deleted, String.valueOf(md.getId()));
            // Get Current Date
            final Calendar c = Calendar.getInstance();
            c.setTimeZone(TimeZone.getTimeZone("UTC"));
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);
            closeHolderIMG.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        dialogTherap.dismiss();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            searchBTN.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        dialogTherap.dismiss();
                        iPaddresS = vehicleNumberET.getText().toString();

                        SharedPrefUtil.setIpaddress(BillingActivity.this, String.valueOf(iPaddresS));

                        //   Toast.makeText(BillingActivity.this, iPaddresS, Toast.LENGTH_SHORT).show();
                        //       getParkingList(vehicleNo, selectedDate);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialogTherap.getWindow();
            lp.copyFrom(window.getAttributes());

            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
            dialogTherap.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialogTherap.show();

            dialogTherap.setOnKeyListener(new Dialog.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_UP) {
                        MyLog.i("@@@@@@@BACK", "PRESSED");
                        dialogTherap.dismiss();
                        return true;
                    }
                    return false;
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}