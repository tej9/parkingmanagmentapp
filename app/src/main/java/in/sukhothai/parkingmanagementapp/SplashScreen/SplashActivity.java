package in.sukhothai.parkingmanagementapp.SplashScreen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import in.sukhothai.parkingmanagementapp.MainActivity;
import in.sukhothai.parkingmanagementapp.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            setContentView(R.layout.activity_splash);
            Thread myThread = new Thread() {
                @Override
                public void run() {
                    try {
                        sleep(2500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        Intent startLoginActivity = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(startLoginActivity);
                        finish();
                    }
                }
            };
            myThread.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}